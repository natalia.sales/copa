export default class Estádio{
  constructor(nome, times){
    this.nome = nome;
    this.times = times;
  }
  possui(){
    return this.nome + ' é o estádio que pertence ao time ' + this.times + '.';
  }
}