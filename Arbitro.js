export default class Arbitro{
  constructor(nome, idade, experiencia, estiloArbitragem){
    this.nome = nome;
    this.idade = idade;
    this.experiencia = experiencia;
    this.estiloArbitragem = estiloArbitragem;
  }
   apita(){

  return ' O arbitro que apita o jogo de hoje é ' + this.nome + ', ele tem ' + this.idade + ' anos de idade. Sua experiência em campo é de  ' + this.experiencia + ' anos. Seu estilo de arbitragem é ' + this.estiloArbitragem + '.';
   }
}
