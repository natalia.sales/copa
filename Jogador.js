export default class Jogador {
  constructor(nome, numero){
    this.nome = nome;
    this.numero = numero;
  }
   num() {
    return 'O número da camisa do capitão ' + this.nome + ' é ' + this.numero + '.' ;
  }
}