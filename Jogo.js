 export default class Jogo{
  constructor(estadio, time1, time2, arb){
    this.estadio = estadio;
    this.time1 = time1;
    this.time2 = time2;
    this.arbitragem = arb; 
  }
  participam(){
      return 'Os times ' + this.time1 + ' e ' + this.time2 + ' jogarão nesta segunda-feira no estádio ' + this.estadio + ' ,terão a participação do juiz ' + this.arbitragem;
  }
}